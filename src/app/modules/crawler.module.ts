import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrawlerRoutingModule } from './crawler-routing.module';
import { CrawlerComponent } from './components/crawler/crawler.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CrawlerFormComponent } from './components/crawler-form/crawler-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { HttpClientModule } from '@angular/common/http';
import { ProxyFormComponent } from './components/proxy-form/proxy-form.component';
import { ConfigurationFormComponent } from './components/configuration-form/configuration-form.component';
import { AgentFormComponent } from './components/agent-form/agent-form.component';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
@NgModule({
  declarations: [CrawlerComponent, CrawlerFormComponent, ProxyFormComponent, ConfigurationFormComponent, AgentFormComponent],
  imports: [
    CommonModule,
    CrawlerRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    MatSelectModule,
    HttpClientModule,
    MatButtonModule,
    MatTableModule,
    MatProgressSpinnerModule,
  ]
})
export class CrawlerModule { }
